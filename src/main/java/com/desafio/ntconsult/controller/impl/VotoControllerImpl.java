package com.desafio.ntconsult.controller.impl;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.desafio.ntconsult.controller.VotoController;
import com.desafio.ntconsult.exception.NotFoundException;
import com.desafio.ntconsult.model.Pauta;
import com.desafio.ntconsult.model.form.RegistroVotoForm;
import com.desafio.ntconsult.service.PautaService;
import com.desafio.ntconsult.service.VotoService;

@RestController
@RequestMapping("/votos")
public class VotoControllerImpl implements VotoController {
	
	@Autowired
	private PautaService pautaService;
	
	@Autowired
	private VotoService votoService;
	
	
	@PostMapping("/v1.0")
	public ResponseEntity<String> votar(@RequestBody @Valid RegistroVotoForm votoForm) {
		Optional<Pauta> pautaOp = pautaService.pesquisarPautaPorId(votoForm.getPautaId());
		
		if(!pautaOp.isPresent())
			throw new NotFoundException("Pauta informada não encontada");
		
		final Pauta pauta = pautaOp.get();
		pautaService.validarSeSessaoPodeSerVotada(pauta);
		votoService.processarVotoAssociadoPauta(pauta, votoForm);
		
		return ResponseEntity.status(HttpStatus.CREATED).body("Voto registrado com sucesso");
	}
	
}
