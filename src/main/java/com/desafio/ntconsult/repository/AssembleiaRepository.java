package com.desafio.ntconsult.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.desafio.ntconsult.model.Assembleia;

@Repository
public interface AssembleiaRepository extends JpaRepository<Assembleia, Long> {
	
}
