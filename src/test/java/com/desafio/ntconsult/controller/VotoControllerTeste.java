package com.desafio.ntconsult.controller;

import java.time.LocalDateTime;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;

import com.desafio.ntconsult.controller.impl.VotoControllerImpl;
import com.desafio.ntconsult.model.Assembleia;
import com.desafio.ntconsult.model.Pauta;
import com.desafio.ntconsult.model.form.RegistroVotoForm;
import com.desafio.ntconsult.service.PautaService;
import com.desafio.ntconsult.service.VotoService;

import io.restassured.http.ContentType;
import io.restassured.module.mockmvc.RestAssuredMockMvc;

@WebMvcTest(controllers = VotoControllerImpl.class)
public class VotoControllerTeste {
	
	@Autowired
	private VotoControllerImpl votoController;
	
	@MockBean
	private VotoService votoService;
	
	@MockBean
	private PautaService pautaService;
	
	private final Long ID = 1L;
	
	
	@BeforeEach
	public void setup() {
		RestAssuredMockMvc.standaloneSetup(this.votoController);
	}
	
	private Pauta construirPauta(LocalDateTime dataInicio, LocalDateTime dataFim) {
		return Pauta.builder()
				.assembleia(Assembleia.builder().id(ID).descricao("Assembleia teste").build())
				.dataInicioVotacao(dataInicio)
				.dataFimVotacao(dataFim)
				.descricao("Pauta teste")
				.id(ID)
				.build();
	}
	
	@Test
	public void deveVotarPauta() {
		final RegistroVotoForm votoForm = RegistroVotoForm.builder()
				.concordaComPauta(true)
				.cpfAssociado("00000000000")
				.pautaId(ID)
				.build();
		final Optional<Pauta> pautaOp = Optional.of(construirPauta(LocalDateTime.now(), LocalDateTime.now().plusMinutes(5)));
		
		Mockito.when(this.pautaService.pesquisarPautaPorId(votoForm.getPautaId())).thenReturn(pautaOp);
		Mockito.when(this.pautaService.validarSeSessaoPodeSerVotada(pautaOp.get())).thenReturn(pautaOp.get());
		
		RestAssuredMockMvc
			.given()
				.body(votoForm)
				.contentType(ContentType.JSON)
			.when()
				.post("/votos/v1.0")
			.then()
				.statusCode(HttpStatus.CREATED.value());
	}

}
