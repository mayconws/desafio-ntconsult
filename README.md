# Controle de Votação #

O presente projeto consiste em demonstrar o funcionamento de uma API, para registrar e contabilizar os votos dos associoados durante as sessões abertas, em assembléias determinadas pela empresa do segmento coperativo.

## Informações e Configurações do Projeto ##

* É Necessário alterar as configurações do arquivo "application.properties" para indicar um banco de dados.
* O projeto está configurado para iniciar através do PostgreSQL:
* Utiliza lombok, necessário instalar na IDE (utilizado Spring Tool Suite 4).
* Iniciar o projeto através da porta 8080.
* Swagger pode ser visualizado através da url http://localhost:8080/swagger-ui.html
* Aplicação está configurada em três versões (1.0, 1.1 e 1.2) para exemplificação de versionamento.

